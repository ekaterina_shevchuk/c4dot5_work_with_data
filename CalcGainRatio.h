#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include"DataMatrix.h"
using namespace std;

class CalcGainRatio {
public:
	CalcGainRatio();
	~CalcGainRatio();
	float Calc_Gain(DataMatrix matrix, string atr);
	float Calc_GainRatio(DataMatrix matrix, string atr);
	float Calc_SplitInfo(DataMatrix matrix, string atr);
	vector<string> GetNamesAtrs();
	vector<float> StartCalc(DataMatrix matrix);


private:
	vector<string> names_atrs;
	vector<float> vals_GR;
};