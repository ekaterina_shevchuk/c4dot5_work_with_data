#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "CalcGainRatio.h"
#include"DataMatrix.h"
using namespace std;

CalcGainRatio::CalcGainRatio() {

}


vector <string> Get_uniq_value(vector<string> value) {
	vector<string> uniq_value;
	int choose = 1;
	for (int i = 0; i < value.size(); i++) {
		choose = 1;
		if (uniq_value.size() == 0) uniq_value.push_back(value[i]);
		else {
			for (int j = 0; j < uniq_value.size(); j++) {
				if (uniq_value[j] == value[i]) {
					choose = 0;
					break;
				}
			}
			if (choose == 1) {
				uniq_value.push_back(value[i]);
			}
		}
	}
	return uniq_value;
}


int * Calc_score(vector<string> value, vector<string> uniq_value) {
	int *score = (int*)calloc(uniq_value.size() + 1, sizeof(int));
	for (int i = 0; i < uniq_value.size(); i++) {
		for (int j = 0; j < value.size(); j++) {
			if (value[j] == uniq_value[i]) score[i]++;
		}
	}
	return score;
}

float CalcGainRatio::Calc_SplitInfo(DataMatrix matrix, string atr) {
	vector <string> value = matrix.Get_value_atr(atr);
	vector<string> uniq_value = Get_uniq_value(value);
	int *score = (int*)calloc(uniq_value.size(), sizeof(int));
	score = Calc_score(value, uniq_value);
	float SplitInfo = 0, tmp = 0;
	int size = value.size();
	for (int i = 0; i < uniq_value.size(); i++) {
		tmp = (float)score[i] / size;
		SplitInfo -= tmp * ((float)log(tmp)) / (log(2));
	}
	return SplitInfo;
}

float Calc_Entropy(vector <string> values_class) {
	vector <string> uniq_values_class = Get_uniq_value(values_class);
	int *score = Calc_score(values_class, uniq_values_class);
	float Entropy = 0, tmp = 0;
	int size = values_class.size();
	for (int i = 0; i < uniq_values_class.size(); i++) {
		tmp = ((float)score[i]) / size;
		Entropy -= tmp * ((float)log(tmp) / log(2));
	}
	return Entropy;
}

float CalcGainRatio::Calc_Gain(DataMatrix matrix, string atr) {
	vector <string> values_class = matrix.Get_example_class();
	vector <string> uniq_value_class = Get_uniq_value(values_class);
	float Entropy = Calc_Entropy(values_class);
	vector <string> value_atr = matrix.Get_value_atr(atr);
	vector <string> uniq_value_atr = Get_uniq_value(value_atr);
	int *score_uniq_atr = Calc_score(value_atr, uniq_value_atr);
	int tmp_size = matrix.Get_num_examples();
	float EntropyAtr = 0, sum_EntropyAtr = 0, tmp_entr = 0, tmp = 0;
	float Gain = Entropy;
	for (int i = 0; i < uniq_value_atr.size(); i++) {
		vector<string> val_class_of_val_atr = matrix.Get_values_class_of_val_atr(atr, uniq_value_atr[i]);
		int * score_uniq = Calc_score(val_class_of_val_atr, uniq_value_class);
		int size = 0;
		tmp_entr = 0;
		for (int i = 0; i < uniq_value_class.size(); i++) size += score_uniq[i];
		for (int j = 0; j < uniq_value_class.size(); j++) {
			tmp = ((float)score_uniq[j]) / size;
			if (tmp == 0) {
				tmp_entr = 0;
			}
			else {
				tmp_entr -= tmp * ((float)log(tmp) / log(2));
			}
		}
		tmp = ((float)score_uniq_atr[i]) / tmp_size;
		Gain -= tmp * tmp_entr;
	}
	return Gain;
}

float CalcGainRatio::Calc_GainRatio(DataMatrix matrix, string atr) {
	return ((float)Calc_Gain(matrix, atr) / Calc_SplitInfo(matrix, atr));
}

vector<string> CalcGainRatio::GetNamesAtrs() {
	return names_atrs;
}

vector<float> CalcGainRatio::StartCalc(DataMatrix matrix) {
	float tmp_GainRatio = 0;
	vector <string> atrs = matrix.Get_atrs();
	for (int i = 0; i < matrix.Get_num_atr(); i++) {
		tmp_GainRatio = Calc_GainRatio(matrix, atrs[i]);
		this->vals_GR.push_back(tmp_GainRatio);
		this->names_atrs.push_back(atrs[i]);
	}

	return vals_GR;
}