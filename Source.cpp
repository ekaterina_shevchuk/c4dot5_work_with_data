#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include "CalcGainRatio.h"
#include "DataMatrix.h"
using namespace std;

void PrintGainRatioDiagram(vector<float> vals_GR, vector<string> names_atrs);
void PrintHistogram(DataMatrix matrix);

int main() {
	DataMatrix* matrix = new DataMatrix("data.txt");
	CalcGainRatio* gr = new CalcGainRatio();
	vector<float> vals_GR = gr->StartCalc(*matrix);
	vector<string> names_atrs = gr->GetNamesAtrs();
	PrintGainRatioDiagram(vals_GR, names_atrs);
	PrintHistogram(*matrix);

	return 0;
}


void PrintGainRatioDiagram(vector<float> vals_GR, vector<string> names_atrs) {
	ofstream out;          
	out.open("GR.txt"); 
	float maxElement = *std::max_element(vals_GR.begin(), vals_GR.end());
	float minElement = *std::min_element(vals_GR.begin(), vals_GR.end());
	float tmp = (float)maxElement / minElement;
	cout << "MAX_ELEM = " << maxElement << "MIN_ELEM = " << minElement << "tmp = " <<  tmp << endl; 
	for (int i = 0; i < names_atrs.size(); i++) {
		if (out.is_open()) {
			out << names_atrs[i] << " = " << vals_GR[i] << " ";
		}
		for (int j = 0; j < 40 - names_atrs[i].size(); j++) {
			out << " ";
		}

		float j = 0;
		while (j < vals_GR[i]){
			j = j + minElement;
			out << "]";
		}
		out << endl;
	}
	cout << " end ";
	return;
}

void PrintHistogram(DataMatrix matrix) {
	vector<string> atrs = matrix.Get_atrs();
	vector<string> value_atrs, uniq_value_atrs;
	bool check = true;
	for (int i = 0; i < atrs.size(); i++) {
		value_atrs = matrix.Get_value_atr(atrs[i]);
		int* freqency = (int*)calloc(value_atrs.size(), sizeof(int));
		for (int k = 0; k < value_atrs.size(); k++) {
			check = true;
			if (k == 0)uniq_value_atrs.push_back(value_atrs[i]);
			for (int j = 0; j < uniq_value_atrs.size(); j++) {
				if (uniq_value_atrs[j] == value_atrs[k]) {
					freqency[j]++;
					check = false;
					continue;
				}
			}
			if (check) {
				uniq_value_atrs.push_back(value_atrs[k]);
				freqency[uniq_value_atrs.size()-1] = 1;
			}
		}
		ofstream out;
		out.open(atrs[i] + ".txt");
		for (int j = 0; j < uniq_value_atrs.size(); j++) {
			out << uniq_value_atrs[j] << " = " << freqency[j] << "        ";

			float k = 0;
			while (k < freqency[j]) {
				k = k + 1;
				out << "]";
			}
			out << endl;
		}
		uniq_value_atrs.erase(uniq_value_atrs.begin(), uniq_value_atrs.end());
		out.close();

	}
	return;

}